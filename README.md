# VERSION CONTROL

# Creazione di una Repository

```
Creare un nuovo repository nella directory corrente: `git init`
Clonare un repository remoto: `git clone [url]`
```

# Branch e Tag

```
Elencare tutti i branch esistenti con il commento dell'ultimo commit: `git branch -av`
Passare al branch specificato: `git checkout [branch]`
Creare un nuovo branch basato sul branch corrente: `git branch [nuovo-branch]`
Creare un nuovo branch di tracciamento basato su un branch remoto: `git checkout --track [remote/branch]`
Eliminare un branch locale: `git branch -d [branch]`
Eseguire il tag del commit corrente: `git tag [nome-tag]`
```

# Cambiamenti Locali

```
Elencare tutti i file nuovi o modificati, mostrando quali sono stati preparati per il commit e quali no: `git status`
Visualizzare le modifiche tra i file preparati e le modifiche non preparate: `git diff`
Visualizzare le modifiche tra i file preparati e l'ultima versione committata: `git diff --cached`
Solo per un file, aggiungi il nome del file: `git diff --cached [file]`
Aggiungere tutte le modifiche correnti al prossimo commit: `git add [file]`
Rimuovere un file dal prossimo commit: `git rm [file]`
Aggiungere alcune modifiche in <file> al prossimo commit: `git add -p [file]`
Committare tutte le modifiche locali nei file tracciati: `git commit -a o git commit -am "Un messaggio"`
Committare le modifiche precedentemente preparate: `git commit o git commit -m "Un messaggio"`
Rimuovere un file dalla fase di preparazione, ma preservarne il contenuto: `git reset [file]`
```

# Cronologia dei Commit

```
Mostrare tutti i commit a partire dal più recente: `git log`
Mostrare le modifiche nel tempo per un file specifico: `git log -p [file]`
Mostrare chi ha modificato ogni riga in un file, quando è stata modificata e l'ID del commit: `git blame -c [file]`
```

# Aggiornamento e Pubblicazione

```
Elencare tutti i repository remoti: git remote -v
Aggiungere un nuovo repository remoto all'URL specificato con il nome locale dato: `git remote add [nome-locale] [url]`
Scaricare tutte le modifiche da un repository remoto, ma non integrarle localmente: `git fetch [remote]`
Scaricare tutte le modifiche remote e unirle localmente: `git pull [remote] [branch]`
Pubblicare le modifiche locali su un repository remoto: `git push [remote] [branch]`
Eliminare un branch sul repository remoto: `git branch -dr [remote/branch]`
Pubblicare i tuoi tag su un repository remoto: `git push --tags`
```

# Merge e Rebase

```
Unire [branch] nel tuo HEAD corrente: `git merge [branch]`
Rebase del tuo HEAD corrente su [branch]: `git rebase [branch]`
Annullare un rebase: `git rebase --abort`
Continuare un rebase dopo aver risolto i conflitti: `git rebase --continue`
Utilizzare il tuo strumento di merge configurato per risolvere i conflitti: `git mergetool`
```

# Annullamento

```
Scartare tutte le modifiche locali e iniziare a lavorare sul branch corrente dall'ultimo commit: `git reset --hard HEAD`
Scartare le modifiche locali a un file specifico: `git checkout HEAD [file]`
Revertire un commit creando un nuovo commit che annulla le modifiche del commit specificato: `git revert [commit]`
Reimpostare il tuo branch corrente a un commit precedente e scartare tutte le modifiche da allora: `git reset --hard [commit]`
Reimpostare il tuo branch corrente a un commit precedente e preservare tutte le modifiche come modifiche non preparate: `git reset [commit]`
Reimpostare il tuo branch corrente a un commit precedente e preservare le modifiche locali preparate: `git reset --keep [commit]`
```


# Gitignore

```
Il file .gitignore è un file di testo che dice a Git quali file e cartelle ignorare in un progetto. Un file .gitignore locale viene tipicamente aggiunto nella cartella root di un progetto.

Per creare un file .gitignore locale, crea un file di testo e dagli il nome .gitignore (ricorda di includere il punto . all’inizio).

Modifica questo file al bisogno. Ogni nuova riga dovrebbe elencare un altro file o un’altra cartella che vuoi che Git ignori.

Le voci in questo file possono anche seguire un pattern: * è un jolly, / è usato per ignorare percorsi relativi al file .gitignore, # è usato per aggiungere commenti al file .gitignore.

Esempio: 
# Ignora i file di sistema Mac
.DS_store

# Ignora la cartella node_modules
node_modules

# Ignora tutti i file .txt
*.txt

# Ignora i file collegati alle chiavi API
.env

# Ignora i file SASS di configurazione
.sass-cache
```